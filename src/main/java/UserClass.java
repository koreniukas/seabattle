import java.util.ArrayList;
import java.util.List;

/**
 * Created by rkore on 10/19/2017.
 */
public class UserClass {

    private String name;
    private List<Ship> userShips;
    private int hitcounter;
    private List <Coordinate> takenShots = new ArrayList<Coordinate>();

    public List<Ship> getUserShips() {
        return userShips;
    }

    public List<Coordinate> getTakenShots() {
        return takenShots;
    }

    public String getName() {
        return this.name;

    }

    public int getHitcounter() {
        return this.hitcounter;
    }

    public void setHitcounter(int hitcounter) {
        this.hitcounter = hitcounter;
    }

    public void saveTakenShots(Coordinate userCoord) {
        this.takenShots.add(userCoord);
    }

    public boolean hasTryedEarlyer(Coordinate coordinate){
        return (this.takenShots.contains(coordinate));
    }

    public UserClass(String name) {
        this.name = name;
    }

    public void printAllShots() {
        System.out.println("Shots you have tryed: ");
        for(Coordinate coordinate: takenShots){
            System.out.println(coordinate.getCol() + coordinate.getRow());

        }
    }

    public ArrayList<Ship> generateUserShips() {


        return null;
    }

}
